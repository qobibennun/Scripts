#!/bin/bash
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #
# Script:
# Description:
# Author: Qobi Ben Nun <qobibennun@gmail.com>
# Version: 0.1
# Date: 04, 2022
# License: GNU/GPL 3.0
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #
# To run this script you just need to type on a terminal emulator
# its name followed by the name o the script you are about to create.
# Please, do not use too long names or spaces for the script name.
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = #
sshfs qobi@tardis:/home/qobi ~/media/qobi
sshfs qobi@tardis:/home/dados ~/media/dados
sshfs qobi@tardis:/home/dados/Mail ~/Mail
exit 0
