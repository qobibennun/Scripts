#!/bin/bash
while true;
do
clear
hour=$(date +%H)
minute=$(date +%M)
second=$(date +%S)
hour_binary=$(echo "ibase=10;obase=2;$hour" | bc)
minute_binary=$(echo "ibase=10;obase=2;$minute" | bc)
second_binary=$(echo "ibase=10;obase=2;$second" | bc)
printf "%06d\n" "$hour_binary"
printf "%06d\n" "$minute_binary"
printf "%06d" "$second_binary"
sleep 1
done
