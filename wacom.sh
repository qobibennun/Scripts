#!/usr/bin/env bash

stylus_dev=$(xsetwacom list | grep STYLUS | awk '{print $7}')
eraser_dev=$(xsetwacom list | grep ERASER | awk '{print $7}')
pad_dev=$(xsetwacom list | grep PAD | awk '{print $7}')

xsetwacom set $stylus_dev MapToOutput eDP-1
xsetwacom set $stylus_dev area 0 0 14760 8303
#xsetwacom set $eraser_dev MapToOutput eDP-1
#xsetwacom set $eraser_dev area 0 0 14760 8303
xsetwacom set $pad_dev Button 1 key +ctrl +shift d -shift -ctrl
xsetwacom set $pad_dev Button 2 key +ctrl +shift r -shift -ctrl
xsetwacom set $pad_dev Button 3 key Delete
xsetwacom set $pad_dev Button 8 key +ctrl z -ctrl
