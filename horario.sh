#!/bin/env bash
# --------------------------------------------------------
# Formatação do Horário de Aulas com os resultados do FET
# para a impressão em PDF.
#---------------------------------------------------------
#
# Este script pode ser usado, modificado e/ou distribuído para qualquer fim.
# Autor: Qobi Ben Nun 
# Copyleft 2020 2021 2022
#
clear
echo -e " ---------------------------------------"
printf " | FORMATAÇÃO DE HORÁRIO DE AULAS \033[1;31m$(date +%Y)\033[0m |\n"
echo -e ' ---------------------------------------'
echo -e "\n Qual Escola?\n 1) EELH\n 2) ETECSUZ.\n"
read -p ' [1/2]: ' qualescola
echo -e ' '

##Escolhendo a Escola
case $qualescola in
	1) escola="EELH"; vimfunc="H"; nomecomp="ESCOLA ESTADUAL PROFESSORA LUIZA HIDAKA";;
	2) escola="ETECSUZ"; vimfunc="E"; nomecomp="Etec de Suzano - Horário Cursos Integrados";;
	*) printf " \033[1;31mPor favor, digite:\n\033[0m 1) para EELH\n 2) para ETECSUZ\n"; exit;;
esac

## Escolhendo a vesão do horário FET
echo -e " ------------------------------- "
echo -e " | Qual é a versão do horário? | "
echo -e " ------------------------------- "
read -p "      : " ver

## Definindo Escola e ano
ano=$(date +%Y)
nome=${escola}${ano}
nomever=${nome}v${ver}
nomever0=${nome}--

## Diretórios de trabalho
base=$HOME/fet-results/timetables
single=$base/${nomever}-single
diretorio=${base}/${nomever}-feito

## Verificando diretórios de trabalho
if [[ -d ${single} ]]; then
	echo -e " ---------------------------------- "
	echo -e " => Diretório SINGLE encontrado. OK"
	echo -e " ---------------------------------- "
	if [[ -d ${diretorio} ]]; then
		echo -e " ---------------------------------- "
		echo -e " => Diretório FEITO já existe!"
		echo -e " ---------------------------------- "
		read -p "Sobrepor? [Sim/Não]: " escolha
		case $escolha in
			[Ss])
				rm -rf ${diretorio}; echo "Continuando...";;
			[Nn])
				echo -e " Saindo...\n";  exit 1;;
			*)
				echo -e " Por favor, digite:\n s para sim\n n para não\n"; exit 1
			;;
		esac
	fi
else
	echo -e "  ---------------------------------- "
	echo -e " => Diretório SINGLE não encontrado."
	echo -e "  ---------------------------------- "
	echo -e " É NECESSÁRIO GERAR HORÁRIO ANTES PELO FET!"
	echo -e " Saindo...\n";  exit 1
fi


## Definindo data de início da vigência do horário
echo -e ' '
echo -e " ---------------------------------------------"
echo -e " | Quando é o início da vigência do horário? |"
echo -e " ---------------------------------------------"
read -p " DD/MM: " vig
echo -e ' '

dia=${vig:0:2}
mes=${vig:3:2}

## Definindo diretórios e arquivos
formato=${base}/formato/${nome}
style00=${formato}/${nome}_stylesheet.css
index00=${formato}/${nome}_index.html
geral00=${formato}/${nome}_geral.html
geral01=${formato}/${nome}_geral1.html
turmas00=${formato}/${nome}_turmas.html
turmas01=${formato}/${nome}_turmas1.html
profs00=${formato}/${nome}_profs.html
profs01=${formato}/${nome}_profs1.html

fet0=${single}/${nomever}_data_and_timetable.fet
index0=${single}/${nomever}_index.html
geral0=${single}/${nomever}_years_time_vertical.html
turmas0=${single}/${nomever}_years_days_horizontal.html
profs0=${single}/${nomever}_teachers_days_horizontal.html

mkdir ${diretorio}

## Compiando arquivos para diretório
echo -e ' '
echo -e " --------------------------------------------------"
echo -e " => Copiando arquivos para diretório\n ${diretorio}..."
echo -e " --------------------------------------------------"
cp -a ${style00} ${diretorio}/${nomever}_stylesheet.css
cp -a ${index00} ${diretorio}/${nomever}_index.html
cp -a ${geral00} ${diretorio}/${nomever}_geral.html
cp -a ${turmas00} ${diretorio}/${nomever}_turmas.html
cp -a ${profs00} ${diretorio}/${nomever}_profs.html
cp -a ${fet0} ${diretorio}/
cp -a ${geral0} ${diretorio}/
cp -a ${turmas0} ${diretorio}/
cp -a ${profs0} ${diretorio}/
cp -a ${turmas0} ${diretorio}/${nomever}_turmas1.html
cp -a ${profs0} ${diretorio}/${nomever}_profs1.html

printf "\033[1;31m Feito!\033[0m\n"


## Trocar nome da Folha de Estilo
echo ' '
echo " -----------------------------------------------------"
echo -e " => Trocando Folha de Estilo"
echo " -----------------------------------------------------"
smodelo=${nome}_stylesheet.css
snovo=${nomever}_stylesheet.css

gmodelo=${nome}_geral.html
gnovo=${nomever}_geral.html

tmodelo=${nome}_turmas.html
tnovo=${nomever}_turmas.html
t1modelo=${nome}_turmas1.html
t1novo=${nomever}_turmas1.html

pmodelo=${nome}_profs.html
pnovo=${nomever}_profs.html
p1modelo=${nome}_profs1.html
p1novo=${nomever}_profs1.html

index=${diretorio}/${nomever}_index.html
geral=${diretorio}/${nomever}_geral.html
geral1=${diretorio}/${nomever}_geral1.html
turmas=${diretorio}/${nomever}_turmas.html
turmas1=${diretorio}/${nomever}_turmas1.html
profs=${diretorio}/${nomever}_profs.html
profs1=${diretorio}/${nomever}_profs1.html

sed -i "s/${smodelo}/${snovo}/" ${index}
sed -i "s/${smodelo}/${snovo}/" ${geral}
sed -i "s/${smodelo}/${snovo}/" ${turmas}
sed -i "s/${smodelo}/${snovo}/" ${turmas1}
sed -i "s/${smodelo}/${snovo}/" ${profs}
sed -i "s/${smodelo}/${snovo}/" ${profs1}

sed -i "s/${gmodelo}/${gnovo}/" ${index}
sed -i "s/${tmodelo}/${tnovo}/" ${index}
sed -i "s/${t1modelo}/${t1novo}/" ${index}
sed -i "s/${pmodelo}/${pnovo}/" ${index}
sed -i "s/${p1modelo}/${p1novo}/" ${index}

printf "\033[1;31m Feito!\033[0m\n"

## Copiando dados para arquivo modelo
cd $diretorio

echo ' '
echo " -----------------------------------------------------"
echo -e " => Copiando dados para modelo GERAL..."
echo " -----------------------------------------------------"

nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}gc()" -c "xa" -p -- ${geral0} ${geral}

printf "\033[1;31m Feito!\033[0m\n"

#echo ' '
#echo " -----------------------------------------------------"
#echo -e " Copiando dados para modelo TURMAS..."
#echo " -----------------------------------------------------"
#
#nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}tc()" -c "xa" -p -- ${turmas0} ${turmas}
#
#printf "\033[1;31m Feito!\033[0m\n"

echo ' '
echo " -----------------------------------------------------"
echo -e " Ajustando dados TURMAS1 e PROFS1..."
echo " -----------------------------------------------------"

sed -i '/^$/d' ${turmas1} ${profs1}
sed -i '1,2 c \<\!DOCTYPE HTML\>' ${turmas1} ${profs1}
sed -i 's/ xmlns\=\"http:\/\/www.w3.org\/1999\/xhtml\"//' ${turmas1} ${profs1}
sed -i '9,/nbsp/d;/caption/d;/FET/d;/topo/d' ${turmas1} ${profs1}
sed -i "/id\=\"table_/i \<header\>\<h2\>${nomecomp} - ${ano}\<\/h2\>\<\/header\>" ${turmas1} ${profs1}
sed -i '/\<header\>/i \<div class=\"tables\"\>' ${turmas1} ${profs1}
sed -i "/\/table>$/a\<footer\> \- ${dia}\/${mes}\/${ano} \- \<\/footer\>" ${turmas1} ${profs1}
sed -i "/\<footer\>/a\<\/div\>" ${turmas1} ${profs1}
sed -i 's/th colspan\=\"5\"/th class\=\"tables\" colspan\=\"5\"/g' ${turmas1} ${profs1}

printf "\033[1;31m Feito!\033[0m\n"
echo ' '
echo " -----------------------------------------------------"
echo -e " => Ajustando dados PROFS1..."
echo " -----------------------------------------------------"

#sed -i '/^$/d' ${profs1}
#sed -i '1,2 c \<\!DOCTYPE HTML\>' ${profs1}
#sed -i 's/ xmlns\=\"http:\/\/www.w3.org\/1999\/xhtml\"//' ${profs1}
#sed -i '9,/nbsp/d;/caption/d;/FET/d;/topo/d' ${profs1}
#sed -i "/id\=\"table_/i \<header\>\<h2\>${nomecomp} - ${ano}\<\/h2\>\<\/header\>" ${turmas1}
#sed -i "/\/table/a \<footer\>A partir de \- ${dia}\/${mes}\/${ano} \- \<\/footer\>" ${profs1}
#sed -i 's/th colspan\=\"5\"/th class\=\"profs\" colspan\=\"5\"/g' ${turmas1}
#printf "\033[1;31m Feito!\033[0m\n"

## Ajustando data de início da vigência do horário
echo ' '
echo " -----------------------------------------------------"
echo -e " => Ajustando data de início da vigência do horário"
echo " -----------------------------------------------------"

sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${index}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${geral}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${geral1}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${turmas}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${turmas1}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${profs}
sed -i -e "s/\- [0-9]\{2\}\/[0-9]\{2\}\/[0-9]\{4\} \-/\- ${dia}\/${mes}\/${ano} \-/g" ${profs1}

printf "\033[1;31m Feito!\033[0m\n"

#exit 0
## Ajustando aulas duplas e triplas
echo ' '
echo " -----------------------------------------------------"
echo -e " => Ajustando aulas duplas"
echo " -----------------------------------------------------"

if [[ ${escola} = 'EELH' ]]; then
ng2m=$(sed -n -E "/INIM/,/FIMM/p" ${geral} | grep -c "rowspan\=\"2\"\ class")
ng2t=$(sed -n -E "/INIT/,/FIMT/p" ${geral} | grep -c "rowspan\=\"2\"\ class")
ng2n=$(sed -n -E "/ININ/,/FIMN/p" ${geral} | grep -c "rowspan\=\"2\"\ class")

nt2=$(sed -n -E '/rowspan\=\"2\"\ class/p' ${turmas1} | grep -c "rowspan\=\"2\"\ class")

nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "sil exec \"norm! ${ng2m}@m\"" -c "xa" -- ${geral}
echo ' '
#echo " -----------------------------------------------------"
printf " GERAL MANHÃ... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "sil exec \"norm! ${ng2t}@t\"" -c "xa" -- ${geral}
echo ' '
#echo " -----------------------------------------------------"
printf " GERAL TARDE... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "sil exec \"norm! ${ng2n}@n\"" -c "xa" -- ${geral}
echo " "
#echo " -----------------------------------------------------"
printf " GERAL NOITE... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"


elif [[ ${escola} = "ETECSUZ" ]]; then 
ng2=$(sed -n -E '/rowspan\=\"2\"\ class/p' ${geral} | grep -c "rowspan\=\"2\"\ class")
echo " "
echo " -----------------------------------------------------"
echo "${ng2} aulas duplas em ${geral}"
echo " -----------------------------------------------------"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}g2()" -c  "sil exec \"norm! ${ng2}@d\"" -c "xa" -- ${geral}
echo " "
echo " -----------------------------------------------------"
printf " GERAL DUPLAS... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

ng3=$(sed -n -E '/rowspan\=\"3\"\ class/p' ${geral} | grep -c "rowspan\=\"3\"\ class")
echo " "
echo " -----------------------------------------------------"
echo "${ng3} aulas triplas em ${geral}"
echo " -----------------------------------------------------"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}g3()" -c  "sil exec \"norm! ${ng3}@t\"" -c "xa" -- ${geral}
echo " "
echo " -----------------------------------------------------"
printf " GERAL TRIPLAS... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

nt2=$(sed -n -E '/rowspan\=\"2\"\ class/p' ${turmas1} | grep -c "rowspan\=\"2\"\ class")
echo " "
echo " -----------------------------------------------------"
echo "${nt2} aulas duplas em ${turmas1}"
echo " -----------------------------------------------------"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}t2()" -c  "sil exec \"norm! ${nt2}@d\"" -c "xa" -- ${turmas1}
echo " "
echo " -----------------------------------------------------"
printf " TURMAS DUPLAS... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

nt3=$(sed -n -E '/rowspan\=\"3\"\ class/p' ${turmas1} | grep -c "rowspan\=\"3\"\ class")
echo " "
echo " -----------------------------------------------------"
echo "${nt3} aulas triplas em ${turmas1}"
echo " -----------------------------------------------------"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}t3()" -c  "sil exec \"norm! ${nt3}@t\"" -c "xa" -- ${turmas1}
echo " "
echo " -----------------------------------------------------"
printf " TURMAS TRIPLAS... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

echo " "
echo " -----------------------------------------------------"
echo -e " => Ajustando nomes das Turmas e Grupos"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}tg()" -c "xa" -- ${geral}
printf " GERAL NOMES TURMAS... \033[1;31m Feito!\033[0m\n"
nvim --headless -c "source ~/.config/nvim/${nome}.vim" -c "call ${vimfunc}tg()" -c "xa" -- ${turmas1}
printf " TURMAS NOMES TURMAS... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"
fi

echo " "
echo " -----------------------------------------------------"
echo -e " => Ajustando cores dos Professores em geral1"
cp -a ${geral} ${geral1}

sed -i -E '/Lázaro/s/class\="c_[0-9]*"/class\="c_001"/' ${geral1}
sed -i -E '/Célia/s/class\="c_[0-9]*"/class\="c_002"/' ${geral1}
sed -i -E '/Cristiane/s/class\="c_[0-9]*"/class\="c_003"/' ${geral1}
sed -i -E '/Callejon/s/class\="c_[0-9]*"/class\="c_004"/' ${geral1}
sed -i -E '/Felipe/s/class\="c_[0-9]*"/class\="c_005"/' ${geral1}
sed -i -E '/MªLuisa/s/class\="c_[0-9]*"/class\="c_006"/' ${geral1}
sed -i -E '/Soninha/s/class\="c_[0-9]*"/class\="c_007"/' ${geral1}
sed -i -E '/Ricardo/s/class\="c_[0-9]*"/class\="c_008"/' ${geral1}
sed -i -E '/Sandra/s/class\="c_[0-9]*"/class\="c_009"/' ${geral1}
sed -i -E '/Adriano/s/class\="c_[0-9]*"/class\="c_010"/' ${geral1}
sed -i -E '/Cláudia/s/class\="c_[0-9]*"/class\="c_011"/' ${geral1}
sed -i -E '/Sem Prof/s/class\="c_[0-9]*"/class\="c_012"/' ${geral1}
sed -i -E '/Thiago/s/class\="c_[0-9]*"/class\="c_013"/' ${geral1}
sed -i -E '/Marli/s/class\="c_[0-9]*"/class\="c_014"/' ${geral1}
sed -i -E '/Douglas/s/class\="c_[0-9]*"/class\="c_015"/' ${geral1}
sed -i -E '/Cesar/s/class\="c_[0-9]*"/class\="c_016"/' ${geral1}
sed -i -E '/Valdir/s/class\="c_[0-9]*"/class\="c_017"/' ${geral1}
sed -i -E '/Vanessa G./s/class\="c_[0-9]*"/class\="c_018"/' ${geral1}
sed -i -E '/Silvana/s/class\="c_[0-9]*"/class\="c_019"/' ${geral1}
sed -i -E '/Simone/s/class\="c_[0-9]*"/class\="c_020"/' ${geral1}
sed -i -E '/Daniel/s/class\="c_[0-9]*"/class\="c_021"/' ${geral1}
sed -i -E '/Daniela/s/class\="c_[0-9]*"/class\="c_022"/' ${geral1}
sed -i -E '/Josiane/s/class\="c_[0-9]*"/class\="c_023"/' ${geral1}
sed -i -E '/Eduardo/s/class\="c_[0-9]*"/class\="c_024"/' ${geral1}
sed -i -E '/Ronaldo/s/class\="c_[0-9]*"/class\="c_025"/' ${geral1}
sed -i -E '/Antenor/s/class\="c_[0-9]*"/class\="c_026"/' ${geral1}
sed -i -E '/Guaraciara/s/class\="c_[0-9]*"/class\="c_027"/' ${geral1}
sed -i -E '/Débora/s/class\="c_[0-9]*"/class\="c_028"/' ${geral1}
sed -i -E '/Anderson/s/class\="c_[0-9]*"/class\="c_029"/' ${geral1}
sed -i -E '/Reginaldo/s/class\="c_[0-9]*"/class\="c_030"/' ${geral1}
sed -i -E '/Vanessa And./s/class\="c_[0-9]*"/class\="c_031"/' ${geral1}
sed -i -E '/Vicente/s/class\="c_[0-9]*"/class\="c_032"/' ${geral1}
sed -i -E '/Alessandra/s/class\="c_[0-9]*"/class\="c_033"/' ${geral1}
sed -i -E '/Daniele/s/class\="c_[0-9]*"/class\="c_034"/' ${geral1}
sed -i -E '/Rita/s/class\="c_[0-9]*"/class\="c_035"/' ${geral1}

printf " Cores dos professores OK em ${geral1} ... \033[1;31m Feito!\033[0m\n"
echo " -----------------------------------------------------"

printf "\033[1;31m PRONTO!\033[0m\n"

exit 0
