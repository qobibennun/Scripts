#!/bin/bash
#
# Math game to help my kid training arithmetics
# This script can be modified and distributed as one wishes.
clear

echo ""
echo ""
echo ""
echo "============================================================"
echo "=  Bem-vinda criança inteligente que gosta de matemática!  ="
echo "============================================================"
echo ""
echo ""
read -p "                Vamos começar? [s/N]" welcome 

clear

case $welcome in
	s) echo "continuar daqui" ;;
	n) echo "Até a próxima!"; exit ;;
	*) exit ;;
esac

exit

